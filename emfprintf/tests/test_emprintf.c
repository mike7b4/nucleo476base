#ifdef UNITTEST_LOCAL
#include <stdio.h>
#include <string.h>
#include "../emfprintf.h"
#define TEST_EQUAL_INT(a, b, desc) {\
	int value = (a); \
	if (value == (b)) \
		printf("%s Result: %u (0x%X) \e[32mOK\e[39m\n", (desc), value, value); \
	else {\
		printf("%s Result: %u (0x%X) \e[31mERROR\e[39m\n", (desc), value, value); \
		return -1; \
	} \
}

#define TEST_EQUAL_STRING(a, b, desc) {\
	if (strcmp(a, b) == 0) \
		printf("%s Result string is: %s \e[32mOK\e[39m\n", (desc), a); \
	else {\
		printf("%s Result string is: %s \e[31mERROR\e[39m\n", (desc), a); \
		return -1; \
	} \
}

typedef struct _Mem {
	uint8_t buf[255];
	uint8_t index;
}Mem;
Mem mem = {.index = 0};

bool cb_put(EMSTREAM *pstream, uint8_t byte)
{
	if (mem.index == 255)
		return false;

	mem.buf[mem.index++] = byte;
	mem.buf[mem.index] = 0;
	return true;
}

int
main(void)
{
	EMSTREAM stream = {.identifier = 1,
						.put_byte = cb_put};

	printf("==== EMPRINT UNITTEST htoi() ====\n");

	TEST_EQUAL_INT(htoi("F"), 15, "htoi(F)");
	TEST_EQUAL_INT(htoi("80"), 128, "htoi(80)");
	TEST_EQUAL_INT(htoi("FI"), 15, "htoi(FI) = F");
	TEST_EQUAL_INT(htoi("FF"), 255, "htoi(FF)");
	TEST_EQUAL_INT(htoi("FFF"), 0xFFF, "htoi(FFF)");
	TEST_EQUAL_INT(htoi("F12"), 0xF12, "htoi(F12)");
	TEST_EQUAL_INT(htoi("12F"), 0x12F, "htoi(12F)");
	TEST_EQUAL_INT(htoi("8000000F"), 0x8000000F, "htoi(8000000F)");
	TEST_EQUAL_INT(htoi("0000000F"), 0xF, "htoi(0000000F)");
	TEST_EQUAL_INT(htoi("FFFFFFFF"), 0xFFFFFFFF, "htoi(FFFFFFFF)");
	TEST_EQUAL_INT(htoi("0"), 0, "htoi(0)");
	TEST_EQUAL_INT(htoi("DFFFFFFFF123"), 0xDFFFFFFF, "htoi(DFFFFFFFF123) cut to 0xDFFFFFFF");

	printf("==== EMPRINT UNITTEST em_printf() ====\n");
	mem.index = 0;
	em_printf(&stream, "Hello World");
	TEST_EQUAL_STRING(mem.buf, "Hello World", "Standard string");

	mem.index = 0;
	em_printf(&stream, "%d", 12345678);
	TEST_EQUAL_STRING(mem.buf, "12345678", "Integer");

	mem.index = 0;
	em_printf(&stream, "Hello %d", 12345678);
	TEST_EQUAL_STRING(mem.buf, "Hello 12345678", "String Integer");

	mem.index = 0;
	em_printf(&stream, "Hello %d %s", 1234, "Goodbye");
	TEST_EQUAL_STRING(mem.buf, "Hello 1234 Goodbye", "String formated int and string");

	mem.index = 0;
	em_printf(&stream, "%0X", 0x001122D3);
	TEST_EQUAL_STRING(mem.buf, "1122D3", "Hex string");

	mem.index = 0;
	em_printf(&stream, "Hex: %0X Decimal: %d", 0x001122D3, 0x001122D3);
	TEST_EQUAL_STRING(mem.buf, "Hex: 1122D3 Decimal: 1123027", "Hex string");

	mem.index = 0;
	em_printf(&stream, "%0X", 0x01);
	TEST_EQUAL_STRING(mem.buf, "01", "Hex string");

	mem.index = 0;
	em_printf(&stream, "%0X", 0x0);
	TEST_EQUAL_STRING(mem.buf, "00", "Hex string");

	mem.index = 0;
	em_printf(&stream, "%j : %j", "key", "value");
	TEST_EQUAL_STRING(mem.buf, "\"key\" : \"value\"", "format %j");

	return 0;
}
#endif
