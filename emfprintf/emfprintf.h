/**
 * File: emfprintf.h
 * Maintainer(s):
 * Mikael Hermansson <mike@7b4.se>
 *
 * Copyright 2013-2015
 *
 * License: BSD
 *
 */

#ifndef __EMFPRINTF_H__
#define __EMFPRINTF_H__
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <stddef.h>

typedef struct _EMSTREAM EMSTREAM;
typedef bool (*put_byte_func)(EMSTREAM *stream, uint8_t character);
typedef int (*read_func)(EMSTREAM *stream, void *data, size_t length);
typedef int (*write_func)(EMSTREAM *stream, const void *data, size_t length);

struct _EMSTREAM {
	/* used as lookup */
	uint8_t identifier;
	void *p_data;
	put_byte_func put_byte;
	read_func read;
	write_func write; // if NULL it will fallback to put
};

/* EXAMPLE  */
/*
 * static cb_put_byte(EMSTREAM *stream, uint8_t byte) { USART_PUT_BYTE(byte); }
   EMSTREAM stream = {
         .identifier=1,
         .p_data = NULL,
         .put_byte = cb_put_byte,
         .write = NULL,
         .read = NULL,
   }
   em_printf(&stream, "Hello World");
*/


int em_read(EMSTREAM *p_stream, void *data , size_t size);
int em_write(EMSTREAM *p_stream, const void *data , size_t size);
int em_scanf(EMSTREAM *p_stream, const char *data, ... );
int em_printf(EMSTREAM *p_stream, const char *format, ... );
uint32_t htoi(char chars[]);

#endif /* end of __EMFPRINTF_H__ */
