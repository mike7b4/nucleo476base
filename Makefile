all:
	../genmake/src/genmake.py --set-target nucleo
	make -C build/nucleo

clean:
	rm -rf build

.PHONY: clean all

